@extends('layouts.master')
@section('title')
    Halaman Tambah Peran
@endsection
@section('sub-title')
    Tambah peran
@endsection
@section('content')
<form action="/cast" method="POST">
    @csrf
    <div class="form-group">
      <label >Nama Peran </label>
      <input type="text" name="name" class="form-control">
    </div>
    @error('name')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label >Umur </label>
        <input type="text" name="umur" class="form-control">
      </div>
      @error('umur')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror

    <div class="form-group">
      <label>Deskripsi Peran </label>
      <textarea name="bio" class="form-control" cols="30" rows="10"></textarea>
      
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    
   
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection