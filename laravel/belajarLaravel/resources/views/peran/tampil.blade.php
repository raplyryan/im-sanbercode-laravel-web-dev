@extends('layouts.master')
@section('title')
    Halaman Tampil Peran
@endsection
@section('sub-title')
    Tampil  peran
@endsection
@section('content')
<a href="/cast/create" class="btn btn-primary btn-sm ">Tambah Kategori</a>

<table class="table table-bordered">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Name</th>
        <th scope="col">aksi</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key=>$item)
        <tr>
            <td>{{$key +1}}</td>
            <td>{{$item->name}}</td>
            <td>
   
                <form action="/cast/{{$item->id}}" method="POST">
                    @csrf
                    @method('delete')
                    <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">edit</a>
                <input type="submit" value='delete' class="btn btn-danger btn-sm">
                </form>

            </td>
        </tr>
            
        @empty
           <tr>
            <td>Data Cast Kosong</td>
        </tr> 
        @endforelse
      
    </tbody>
  </table>

@endsection

