<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'dashboard']);


Route::get('/register',[AuthController::class,'register']);

Route::post('/welcome',[AuthController::class,'kirim']);

Route::get('/data-table',function(){
    return view('page.data-table');
});

Route::get('/table',function(){
    return view('page.table');
});


//CRUD
//create data untuk mengarah ke form cast
Route::get('/cast/create',[CastController::class,'create']);

//route untuk menyimpan kedalam database cast
Route::post('cast',[CastController::class,'store']);

//Read Data yang ada di table cast
Route::get('/cast',[CastController::class,'dashboard']);

//Read Detai berdasarkan data cast
Route::get('/cast/{id}',[CastController::class,'show']);

//update
//rute mengarah ke form edit cast
Route::get('cast/{id}/edit',[CastController::class,'edit']);

//Rute utk edit data berdasarkan cast
Route::put('/cast/{id}',[CastController::class,'update']);

//delete data
Route::delete('/cast/{id}',[CastController::class,'destroy']);


