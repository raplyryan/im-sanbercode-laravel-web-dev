<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function dashboard()
    {
        $cast = DB::table('cast')->get();
        //dd($cast);
 
        return view('peran.tampil', ['cast' => $cast]);    
    }

    public function create()
    {
        return view('peran.tambah');
    }
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        DB::table('cast')->insert([
            'name' => $request['name'],
            'umur'=> $request['umur'],
            'bio' => $request['bio']
        ]);

        return redirect('/cast');

    }
    public function show($id){
        $cast = DB::table('cast')->find($id);
        return view('peran.detail',['cast'=>$cast]);
    }
    public function edit($id){
        $cast = DB::table('cast')->find($id);
        return view('peran.edit',['cast'=>$cast]);
    }
    public function update($id,Request $request){
        $request->validate([
            'name' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);
        DB::table('cast')
        ->where('id',$id)
        ->update([
            'name'=>$request['name'],
            'umur'=>$request['umur'],
            'bio'=>$request['bio']
        ]);
        return redirect('cast');
    }
        public function destroy($id){
           DB::table('cast')->where('id','=',$id)->delete();   
           return redirect('/cast');
        }
      
    }

