Soal No.1 
Buatlah database dengan nama “myshop”. Tulislah di text jawaban pada nomor 

create database myshop;

---------------------------------------------------------------------
---------------------------------------------------------------------

Soal no.2
Buatlah tabel – tabel baru di dalam database myshop sesuai data-data berikut:
-users (id int,name,email,password)
-categories (id,name)
-items (id,name,description,price,stock,category_id)

//table users
create table users(
    id int(4) PRIMARY KEY AUTO_INCREMENT,
    name varchar(255) NOT null ,
    email varchar(255) NOT null , 
    password varchar(255) NOT null
    );
    
//table categories
create table categories(
    id int(8) PRIMARY KEY AUTO_INCREMENT,
    name varchar(255) NOT null
    );

//table items
create table items(
    id int(8) PRIMARY KEY AUTO_INCREMENT,
    name varchar(255),
    description varchar(255),
    price int(10),
    stock int(10),
    categories_id(8) NOT null,
    FOREIGN KEY(categories_id) REFERENCES categories(id)
    );

----------------------------------------------------------------------
----------------------------------------------------------------------

Soal no.3
Masukkanlah data data berikut dengan perintah SQL “INSERT INTO . . ” ke dalam table yang sudah dibuat pada soal sebelumnya.

//table users
INSERT into users(name,email,password) values ("John Doe","john@doe.com","john123"), ("Jane Doe","jane@doe.com","jenita123");

//table categories
insert into categories(name) values ("gadget"),("cloth"),("men"),("women"),("branded");

//table items
INSERT INTO items(name,description,price,stock,categories_id) values ("sumsang b50","hape keren dari merek sumsang",4000000,100,1) , ("uniklooh","baju keren dari brand ternama",500000,50,2) ,("imho watch","jam tangan anak yang jujur banget",2000000,10,1);  

-----------------------------------------------------------------------
-----------------------------------------------------------------------
Soal no.4

a)Buatlah sebuah query untuk mendapatkan data seluruh user pada table users. Sajikan semua field pada table users KECUALI password nya.

select name,email from users;

b)
Buatlah sebuah query untuk mendapatkan data item pada table items yang memiliki harga di atas 1000000 (satu juta).

select * from items where price  > 1000000

Buat sebuah query untuk mengambil data item pada table items yang memiliki name serupa atau mirip (like) dengan kata kunci “uniklo”, “watch”, atau “sang” (pilih salah satu saja).

SELECT * FROM items where name like "%sang%"

c)SELECT items.*,categories.name as categories from items INNER JOIN categories on items.categories_id = categories.id;

----------------------------------------------------------------------
----------------------------------------------------------------------
Soal no.5
Ubahlah data pada table items untuk item dengan nama sumsang b50 harganya (price) menjadi 2500000. Masukkan query pada text jawaban di nomor ke 5.

update items set price = 2500000 where id = 1;
